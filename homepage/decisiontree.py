import numpy
import pandas as pd
import os
from django.conf import settings


column_name = {'usia' : 'Usia', 'jenis_kelamin' : 'Jenis Kelamin', 'berat_badan' : 'Berat Badan',
                 'tinggi_badan': 'Tinggi Badan', 'makan_buah': 'Makan Buah', 'makan_sehari': 'Makan per Hari',
                 'sayur': 'Makan Sayur', 'minum': 'Banyak Minum', 'jumlah_bab': 'Jumlah BAB', 'olahraga': 'Olahraga',
                 'aktivitas_fisik': 'Aktivitas Fisik', 'tidur': 'Tidur', 'stress': 'Stress'}


def is_numeric(value):
    return isinstance(value, int) or isinstance(value, float) or isinstance(value, numpy.int64)


def classify_bt(data, node, traversed_node):
    if isinstance(node, LeafNodeBT):
        decisive_node = find_decisive_node(data, node.prediction, traversed_node)
        return node.prediction, column_name[decisive_node]

    question = node.question
    traversed_node.append(question)

    if question.match(data):
        return classify_bt(data, node.true_branch, traversed_node)
    else:
        return classify_bt(data, node.false_branch, traversed_node)

def find_decisive_node(data, prediction, traversed_node):
    max_count = 0
    decisive_node = None
    sample_data = pd.read_csv(os.path.join(settings.BASE_DIR, 'homepage/static/healthcheckdata.csv'))

    count = 0
    for question in traversed_node:
        column = question.column
        value = question.value
        
        if column in ['Usia', 'Jenis Kelamin']:
            continue
        
        if is_numeric(value):
            if question.match(data):
                count = sum((sample_data[column] >= value) & (sample_data['prediction'] == prediction))
            else:
                count = sum((sample_data[column] < value) & (sample_data['prediction'] == prediction))
        
        else:
            if question.match(data):
                count = sum((sample_data[column] == value) & (sample_data['prediction'] == prediction))
            else:
                count = sum((sample_data[column] != value) & (sample_data['prediction'] == prediction))

        if count > max_count:
            max_count = count
            decisive_node = question

    return decisive_node.column


def dict_to_node(node_dict):
    if not isinstance(node_dict, dict):
        return LeafNodeBT(node_dict)

    q_dict = node_dict["question"]
    if q_dict["value"].isnumeric():
        q_dict["value"] = int(q_dict["value"])

    question = Question(q_dict["column"], q_dict["value"])

    return DecisionNodeBT(
        question,
        dict_to_node(node_dict["true_branch"]),
        dict_to_node(node_dict["false_branch"])
    )


class DecisionNodeBT:
    def __init__(self, question, true_branch, false_branch):
        self.question = question
        self.true_branch = true_branch
        self.false_branch = false_branch


class LeafNodeBT:
    def __init__(self, prediction):
        self.prediction = prediction


class Question:
    def __init__(self, column, value):
        self.column = column
        self.value = value

    def match(self, data):
        data_value = data[self.column]
        if is_numeric(data_value):
            return data_value >= self.value
        else:
            return data_value == self.value

    def to_dictionary(self):
        if is_numeric(self.value):
            return {
                "column": self.column,
                "value": str(self.value)
            }
        return {
            "column": self.column,
            "value": self.value
        }

    def __repr__(self):
        condition = "=="
        if is_numeric(self.value):
            condition = ">="
        return "Is %s %s %s?" % (
            self.column, condition, str(self.value))
